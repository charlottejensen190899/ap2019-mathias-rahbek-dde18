![ScreenShot](https://cdn.staticaly.com/gl/mathiasrahbek96/ap2019-mathias-rahbek-dde18/raw/master/MiniEx6/example.png)

[RUNME](https://cdn.staticaly.com/gl/mathiasrahbek96/ap2019-mathias-rahbek-dde18/raw/master/MiniEx6/empty-example/index.html)

**About my code**

I have made a very simple basketball game. It consists of basketballs dropping randomly from the top of the screen, and then you get a point if you are fast enough to move the basket under the ball before impact. If you miss, the score resets to zero and you can start over again and try to beat your high score. Unfortunately, you have to keep track of that yourself. 

To make these random appearing objects (basketballs) I made a class on a separated .js file in which I defined different constants in the constructor. I wanted to make the basketballs appear randomly on the x-axis *this.xpos = random(this.size / 2, width - this.size / 2),* and always from the top of the screen *this.ypos = 0.* I gave the basketballs the default diameter of 60 pixels *this.size = 60* and made them fall at a rapidly increasing velocity *this.velocity = 0.25.*
I used a for-loop to make a new ball spawn whenever the other ball had reached the bottom 
  
*if (basketballs[i].ypos > height) {*

 *basketballs.push(new Basketball());*
 
 *basketballs.splice(i, 1);*
 
 *}*

  

I believe that having this on a separate file gave me a better overview of my overall source code, however getting it all to work the way I wanted was a difficult task. What messes with my mind is especially all of these this.dot constants and various functions. And even though it is meant to be better organized this way, it still just sometimes seems like everything is in disorder and I just get confused about what they mean and how much I need to clarify first before using them, and also where to write them. But I guess you learn to manage this clutter and learn to see the structure inside of the “mess” through practice.

The characteristics of object-oriented programming are basically that you, through classes and often arrays, are able to create objects fast and in the proportions you desire. And through these classes which functions as blueprints, you also have the ability to make smaller variations of your objects. So, the main point about this is to save time and use a more efficient way of creating objects.

In generative art, I believe you often use object-oriented programming to create the artwork. Here it is basically the computer that becomes the artist. Your job as the programming artist is to add various objects with various shapes in various sizes that are programmed to behave, move and appear in various ways and in various color schemes. After that is done, the program will draw the image itself, so to speak. That is a very interesting contrast of how things have turned around. Because here the computer is the one acting the right arm and the artist is becoming the brain that is giving instructions for the right arm to draw on the canvas. And I believe that this switch of roles is very fascinating, but also somewhat alarming in the sense that we might not know the real consequences of giving technology this room of operation. I believe that technology should always just be tools and never exchanged for the unique and creative human mind. What troubles me is if AI one day will become as smart as the human mind. And that the roles once again will be switched around but now with us as arms and the computer as the brain.
