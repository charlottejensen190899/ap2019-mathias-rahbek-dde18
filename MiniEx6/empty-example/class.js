class Basketball {
  constructor() {
    this.ypos = 0;
    this.size = 60;
    this.xpos = random(this.size / 2, width - this.size / 2);
    this.velocity = 0.25
  }

  move() {
    this.velocity += 0.25;
    this.ypos = this.ypos + this.velocity;
  }

  show() {
    strokeWeight(0.5)
    stroke(0)
    fill(220, 90, 0);
    ellipse(this.xpos, this.ypos, this.size, this.size);
  }
}
