![ScreenShot](https://cdn.staticaly.com/gl/mathiasrahbek96/ap2019-mathias-rahbek-dde18/raw/master/MiniEx4/example1.png)
![ScreenShot](https://cdn.staticaly.com/gl/mathiasrahbek96/ap2019-mathias-rahbek-dde18/raw/master/MiniEx4/example2.png)
![ScreenShot](https://cdn.staticaly.com/gl/mathiasrahbek96/ap2019-mathias-rahbek-dde18/raw/master/MiniEx4/example3.png)

[Runme](https://cdn.staticaly.com/gl/mathiasrahbek96/ap2019-mathias-rahbek-dde18/raw/master/MiniEx4/empty-example/index.html)

Note: Open with Chrome

**My Coding Process**

My program is built upon the idea of entertaining yourself and others. It shows a television and two speakers. On the television, there are two ‘ON’ and ‘OFF’ buttons which are mapped to turn on and off your webcam. The speakers were also made to be interactive by making the volume input of the microphone, define the height and width of the ellipses that act as speaker membranes. However, this apparently only worked in Google Chrome. Besides that, I have made a scenery mode through ‘if’ and ‘else’ operators that change the background when pressing the space bar and returns to the default background when pressing any other key. It should give the effect of dimming the lights in your room.

My conceptual thought with this program is primarily to rearrange the focus you have on entertainment. To let the source of entertainment, originate from yourself and your own personal contributions so that you become the entertainer for yourself and for others. It is not meant in the sense of ridiculing your appearance and looks, but more to showcase the uniqueness you contribute with in this world as a human being, through your gifts and skills. To express yourself creatively and inspire others around you in showing you as a unique person. And that this thought should be imbued in all aspects of your life through every day, work, fun, etc. This thought should be captured when you appear on the screen and through the speakers.

Besides building my program from these conceptual thoughts, I also saw it as a chance of experimenting with various data capturing inputs. As written earlier, I’ve used buttons, audio, video, and keyboard. Some of these inputs were more difficult to work with than others. The audio part fx were very tricky when trying to create some kind of interactivity with it. But I ended up very satisfied with the result, making a great illusion of a real speaker membrane vibrating. The downside, however, was that in order to make the scenery mode, I had to move the canvas background into setup, which meant that if the membrane (ellipse) went beyond the speaker it would draw circles on the canvas because it is placed in the draw function. I guess I could have spent a little more time finding a solution for that.
Another thing I spent a lot of time on was to program the ‘OFF’-button to turn off the camera again. But what made it work here was to make the capture-variable global instead of local, so that more than one function could call upon it.

So, the coding process was quite a bit more challenging than the previous mini exercises, sometimes even more frustrating as well. But it is also a very nice thing to learn to use, because of the aspect of interactivity it includes. But it also frightens me a bit how much data capture is part of our daily lives. Large parts of what defines us personally are captured as data and used within a digital world. So, I guess you can say that along with digitalization comes dehumanization, and this mini ex has definitely visualized that aspect for me as well.
