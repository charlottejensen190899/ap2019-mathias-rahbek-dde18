![ScreenShot](https://cdn.staticaly.com/gl/mathiasrahbek96/ap2019-mathias-rahbek-dde18/raw/master/MiniEx3/throbbermill1.png)
![ScreenShot](https://cdn.staticaly.com/gl/mathiasrahbek96/ap2019-mathias-rahbek-dde18/raw/master/MiniEx3/throbbermill2.png)

[RUN PROGRAM](https://cdn.staticaly.com/gl/mathiasrahbek96/ap2019-mathias-rahbek-dde18/raw/master/MiniEx3/empty-example/index.html)
NOTE: Run it in fullscreen

[CODE](https://cdn.staticaly.com/gl/mathiasrahbek96/ap2019-mathias-rahbek-dde18/raw/master/MiniEx3/empty-example/sketch.js) 

**About my code**



My program is supposed to visualize an alternative throbber. It illustrates a windmill from old days and has this retro design with brown and nicotine white colors. The interaction with the mouse cursor is imitating the wind that should make the wings turn on the windmill. And while you’re making it spin, the loading progress increases. In that way, you become the one responsible for the program to load, so to speak. It should give connotations to old days where hard work and manual actions were required to run things.
I have used various geometric shapes such as ellipses, rectangles, and curveVertex to create the mill. I have used the “AND”, “if” and “else” control structures to outline the room wherein the mouseMoved function operates. And also to create the progress bar with its different features when loading, and when done loading, to change colors, saturation, and text. These parts were definitely the most tricky and time consuming ones. I also tried to use the array function, but could not get it to work the way I wanted, nor was it suitable for the vision I had with my program.

A throbber is basically the pretty facade shown outwardly, hiding the filthy executions of raw data, highly advanced computations and micro-temporal processes. However, I do think that throbbers have a great effect on making a natural transition in this “waiting room”. When you see a throbber, you know that the loading is in progress and you can relax and wait for the program to open. You know that you have done your part, and you are now forced to wait. That is simply the only option you have. If you don’t see a throbber however, you become stressed because you don’t know if the program needs further of your attention to open. Therefore, I think we should appreciate throbbers. One could even say that they are the little breathing holes in a stressed everyday life. 

One of the ideas with my throbber was to make this loading process and waiting room a little more interesting and fun to attend by gamifying it with interactions. Of course, you cannot load the computations of a program by moving a cursor, but it’s the fact that you get to interact with the throbber instead of just observing it. You get to feel like you are the one executing the computations in the machine for the program to run. By gamifying it, the goal is also to affect the cognitive perception of time and space, so your frustration of waiting gets drowned out by the joy and excitement you might receive through this interaction.

Time is one of the most important parameters when talking about throbbers. And the fact that we actually waste a lot of time waiting for programs to load is a great reality. The world we live in becomes more and more digitalized which also means that we might end up wasting even more time waiting in the future. Therefore, we should start thinking about how we can use this waiting time more efficient – think of alternatives to waiting. My suggestion is hereby gamification of throbbers.
