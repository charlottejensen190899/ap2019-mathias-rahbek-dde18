let value = 0.02
let rectw = 0.1

function setup() {
  createCanvas(windowWidth, windowHeight);
}

function draw() {
  background(255, 200, 100);
  rectMode(CENTER);
  translate(width / 2, height / 2);


  //mill tower
  fill(100, 50, 0);
  beginShape();
  curveVertex(-110, 300);
  curveVertex(-110, 300);
  curveVertex(-70, 50);
  curveVertex(70, 50);
  curveVertex(110, 300);
  curveVertex(110, 300);
  endShape();

  //door in mill tower
  fill(0);
  beginShape();
  curveVertex(-25, 300);
  curveVertex(-25, 300);
  curveVertex(-20, 250);
  curveVertex(0, 230);
  curveVertex(20, 250);
  curveVertex(25, 300);
  curveVertex(25, 300);
  endShape();

  //bottom line of the tower
  push()
  strokeWeight(2);
  line(-110, 300, 110, 300)
  pop()

  //mill house
  fill(50, 20, 0)
  beginShape();
  curveVertex(-70, 50);
  curveVertex(-70, 50);
  curveVertex(-55, -50);
  curveVertex(55, -50);
  curveVertex(70, 50);
  curveVertex(70, 50);
  endShape();

  fill(100, 50)
  rect(0, 0, 60, 60, 20, 20);

  //PROGRESS BAR START//
  push()
  rectMode(CORNER)
  fill(10, 102);
  if (mouseX > 520 && mouseX < 920 && mouseY > 770 && mouseY < 790) {
    fill(50, 50)
  }
  rect(-200, 350, 400, 20, 2);

  //sets a x position limit to the width parameter on the rectangle (progress bar)
  if (rectw < 400) {
    rect(-200, 350, rectw, 20, 2);

    //changes the color of the progress bar and the text when it reaches 100%
    fill(20, 240);
    textSize(32);
    text('Loading ...', -90, -360);
    textSize(16);
    text('If you move the cursor', -60, -340);
  } else {
    fill(200, 200, 0);
    rect(-200, 350, 400, 20, 2);
    textSize(20);
    fill(20, 240)
    text('Loading succesful!', -82, 340);
    textSize(32);
    text('Hooray!', -60, -360);
  }
  pop()
  //PROGRESS BAR END//

  //rotation with and without interactivity
  if (mouseX > 400 && mouseX < 1000 && mouseY > 100 && mouseY < 700) {
    rotate(1 * value);
  }
  rotate(frameCount * 0.002);

  //WINGS

  //arms
  fill(200, 100, 0)
  rect(0, 0, 500, 20, 20);
  rect(0, 0, 20, 500, 20);

  //sails
  fill(250, 160, 0);
  rect(-140, 40, 200, 60);
  rect(-40, -140, 60, 200);
  rect(140, -40, 200, 60);
  rect(40, 140, 60, 200);

  //small dot in the middle of the mill house
  fill(150)
  ellipse(0, 0, 10);

}
//interactivity with the mouse
function mouseMoved() {
  if (mouseX > 400 && mouseX < 1000 && mouseY > 100 && mouseY < 700) {
    value = value + 0.03;
    rectw = rectw + 0.1;
  }
}
